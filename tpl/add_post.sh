#!/bin/bash

#$1 is username
#$2 text

#http://docs.blackened.cz/en-US/BlazeBlogger/1.2.0/html/User_Guide/index.html
#stdin is &3

#add a post with blazeblogger
blaze-add -t "$(date)" -k "$1" -P -E cat <(echo "${2}")

#make
blaze-make -d $HOME/www 
